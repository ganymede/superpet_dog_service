package com.superpet.common.kits;

public class ConstantKit {

    /**
     * Response Code
     */
    public final static String CODE_SUCCESS = "000";//操作成功
    public final static String CODE_FAIL = "101";//服务器报错
    public final static String CODE_WX_FAIL = "102";//访问微信服务器报错
    public final static String CODE_SESSION_OUT = "103";//登录过期
    public final static String CODE_FORBID = "909";//禁止访问
    public final static String CODE_FILEUPLOAD_FAIL = "104";//文件上传失败

    /**
     * Response Message
     */
    public final static String MSG_SUCCESS = "Success";//操作成功
    public final static String MSG_FAIL = "Server Error";//服务器报错
    public final static String MSG_WX_FAIL = "Wx Server Error ";//访问微信服务器报错
    public final static String MSG_SESSION_OUT = "Session Timeout";//登录过期
    public final static String MSG_FORBID = "Access Forbid";//禁止访问
    public final static String MSG_FILEUPLOAD_FAIL = "FIle Upload Fail";//文件上传失败

    /**
     * Cache Name
     */
    public final static String LOGIN_ACCOUNT_CACHE = "loginAccount";//Token
    public final static String WXRUN_STEP_CACHE = "wxRunStep";//昨天的微信运动步数
}
